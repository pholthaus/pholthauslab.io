---
template: cv
title: Curriculum Vitae
subtitle: Patrick Holthaus
documentclass: scrartcl
header-includes: \usepackage{noto}
---

[//]: # (# Personal Information)
[//]: # (* **Address** Welwyn Garden City, UK)
[//]: # (* **Birthday** January 26th, 1984 in Oelde, Germany)
[//]: # (* **Civil status** Married, one daughter)
